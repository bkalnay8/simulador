%---------------------------------------------------------------------%
%						    Simulador QAM-M
%					patricio.reus.merlo@gmail.com
%                       franrainero1@gmail.com
%                      stgoleguizamon@gmail.com
%
% Filename      : main.m
% Programmers   : Patricio Reus Merlo; Francisco Rainero; Santiago Leguizamon
% Created on	: 10 Jul. 2021
% Description 	: digital communications simulator 
%
% BASADO en ese script, con los parametros adecuados para que la
% modulacion sea PAM-2 y el ecualizador trabaje a tasa de baudio (ver
% receiver.m)
%---------------------------------------------------------------------%

%---------------------------------------------------------------------%
%                               IMPORTANT
%                               *********
%    To run the simulator it is necessary to go to the simulator/
%    directory in MATLAB and then, from this point, execute main.m
%---------------------------------------------------------------------%

%---------------------------------------------------------------------%
%                            PATH SETTINGS
%---------------------------------------------------------------------%

addpath(strcat(pwd,'/src/' )) ;  % Add src  path to MATLAB paths
%addpath(strcat(pwd,'/test/')) ;  % Add test path to MATLAB paths

%---------------------------------------------------------------------%
%                           SIMULATOR SETTINGS 
%---------------------------------------------------------------------%

clear;
close all

% TODO: estaria bueno agregar un log que tenga la estructura de config

% utilizada en cada simulacion

% ------------ Simulation settings --------------
config.simulationIterations                 = 1       ; % Must be  >= 1          (not iterable)
config.sim.simulationLength                 = 1e6     ; % Must be  >= 1          (  iterable  )

% --------------- Plots settings ----------------
config.sim.showPlots                        = 1       ; % (1) Yes  : (0) No      (  iterable  )
config.sim.exportPlots                      = 0       ; % (1) Yes  : (0) No      (  iterable  )

% -- TX plots --
config.sim.numberOfSymbolsToShow            = 15      ; % Must be  > 0           (  iterable  )
config.sim.transmittedSymbols               = 0       ; % (1) Yes  : (0) No      (  iterable  )
config.sim.transmittedSignals               = 0       ; % (1) Yes  : (0) No      (  iterable  )
config.sim.transmittedConstellation         = 0       ; % (1) Yes  : (0) No      (  iterable  )
config.sim.transmittedEyeDiagram            = 0       ; % (1) Yes  : (0) No      (  iterable  )

% -- TX vs RX plots --
config.sim.TxRxSpectrum                     = 1       ; % (1) Yes  : (0) No      (  iterable  )

% -- RX plots --
config.sim.receivedSignals                  = 0       ; % (1) Yes  : (0) No      (  iterable  )
config.sim.receivedEyeDiagram               = 0       ; % (1) Yes  : (0) No      (  iterable  )

% TODO: cambiar colores de este plot
config.sim.receivedConstellation            = 0 ; % (1) Yes  : (0) No      (  iterable  )
config.sim.correlatorOutVsSlicerOut         = 0 ; % (1) Yes  : (0) No      (  iterable  )
config.sim.slicerInputHistogram             = 0 ; % (1) Yes  : (0) No      (  iterable  )
config.sim.FSE_Taps_evolution_plot          = 0 ; % (1) Yes  : (0) No
config.sim.ChannelFreqResponse              = 1 ; % (1) Yes  : (0) No
config.sim.FSEFreqResponse                  = 0 ; % (1) Yes  : (0) No
config.sim.ChannelVsFSEqualizer             = 0 ; % (1) Yes  : (0) No
config.sim.FSEvsChannelTaps                 = 0 ; % (1) Yes  : (0) No

% TODO: Agregar plot del espectro plegado (Lo mostro nestor en una de las clases)
% TODO: Agregar plot de los filtros anti alias y el filtro del canal (superpuesto a las psd del ruido y señal)
% TODO: Agregar guarda a los plots para no ver la convergencia del FSE + CMA

% --------------- Logs settings -----------------
% TODO: Guardar BER y SER con una guarda para no contar en la convergencia del FSE
config.sim.exportLogs                = 1      ; % (1) Yes  : (0) No      (  iterable  )
config.sim.exportSER                 = 0      ; % (1) Yes  : (0) No      (  iterable  )
config.sim.exportBER                 = 0      ; % (1) Yes  : (0) No      (  iterable  )
config.sim.exportBERVsEbNoVsChBW     = 0      ; % (1) Yes  : (0) No
config.sim.exportBERVsEbNoVsNTapsFSE = 0      ; % (1) Yes  : (0) No

% ---- Digital communications system settings ---
config.sys.modulationType           = 0       ; % (1) QAM  : (0) PAM     (  iterable  )
config.sys.symbolRate               = 32e9    ; % Must be  >  0          (  iterable  )
config.sys.mapperLevels             = 2       ; % Must be =   2,4 or 8   (  iterable  )
config.sys.oversamplingFactor       = 4       ; % TX and CH factor       (  iterable  )
config.sys.dspOversamplingFactor    = 1       ; % RX (FSE factor         (  iterable  )

% ------------ Transmitter settings -------------
config.tx.filterType                = 1       ; % (1) RRC  : (0) RC      (  iterable  )
config.tx.filterLength              = 150     ; % Must be  >  1          (  iterable  )
config.tx.filterRolloff             = 0.1     ; % Must be 0 < x <= 1     (  iterable  )

% -------------- Channel settings ---------------
% -- Noise --
config.ch.addNoise                  = 1     ; % (0): No                (  iterable  )
                                              % (1): Before channel transfer function
                                              % (2): After channel transfer function
%config.ch.EbNo_dB                   = [0.5:0.5:10, 0.5:0.5:10, 0.5:0.5:10, 0.5:0.5:10, 0.5:0.5:10, 0.5:0.5:10, 0.5:0.5:10, 0.5:0.5:10]    ; % Must be real           (  iterable  )
config.ch.EbNo_dB                   = 5;

% -- ISI --
config.ch.bandwidthType             = 1         ; % (0): No                (  iterable  )
                                                  % (1): Low Pass Filter
                                                  % (2): Custom transfer function
                                                  
config.ch.filterLength              = 21        ; % Must be  >  1          (  iterable  )
config.ch.filterCutFrequency        = 16e9      ; % [ones(1,20)*17e9, ones(1,20)*18e9] ; % Channel BW in Hz       (  iterable  )
% config.ch.channelTaps               = [[2, 7]]   ; % Pasar taps entre corchetes config.ch.channelTaps = [ [1 -2] ] 
% FIXME: arreglar iteraciones con channelTaps


% ------------- Receiver settings ---------------

% -- Down converter --
config.rx.dwConvPhaseError          = 0       ; % [deg]                     (  iterable  )
config.rx.dwConvfrequencyOffset     = 0       ; % [Hz]                      (  iterable  )

% -- Anti-alias filter --
config.rx.antialiasEnable           = 0         ; % (1) Yes  : (0) No       (  iterable  )
config.rx.antialiasLength           = 101       ; % Must be  >  1           (  iterable  )
config.rx.antialiasCutFrequency     = 31.5e9    ; % 0 < fc < 1              (  iterable  )

% --Matched filter --
config.rx.mfEnable                  = 1       ;
config.rx.hTaps                     = conv(raiz_cos_realzado(config.sys.symbolRate, config.sys.oversamplingFactor*config.sys.symbolRate,config.tx.filterRolloff, config.tx.filterLength), ...
                                      fir1(config.ch.filterLength, config.ch.filterCutFrequency/(config.sys.oversamplingFactor*config.sys.symbolRate/2)),'same');
                                  % Coeficientes de la respuesta
                                  % equivalente del filtro conformador de
                                  % pulso y el canal

% -- ADC --
config.rx.adcInitialPhase           = 0       ; %                           (  iterable  )

% -- Automatic gain control --
% FIXME: para QAM64 hay que tunear bien esto sino da cualquier cosa
config.rx.agcOutputLevel            = sqrt(2) ; % [rms]                     (  iterable  )

% -- FSE --
config.rx.fseLength                 = 63      ; % Must be  >  1           (  iterable  )
config.rx.saveFilterTaps            = 0       ; % (1) Yes : (0) No


% -- LMS --
config.rx.lmsStepCMA                = 2^(-11) ; % LMS step with CMA         (  iterable  )
config.rx.lmsStepDecisionDriven     = 2^(-13) ; % LMS step with DD          (  iterable  )
config.rx.lmsTapsLeakage            = 10e-2    ; % Taps forgetting factor   (  iterable  )

% -- CMA --
config.rx.forceCMA                  = 0       ; % (1) Yes  : (0) No         (  iterable  )
config.rx.symbolsWithCMAEnabled     = 3e5     ; % Symbols quantity          (  iterable  )

%---------------------------------------------------------------------%
%                                RUN 
%---------------------------------------------------------------------%
tic
sim = simulator(config);
sim.run();
toc
%---------------------------------------------------------------------%
%                           SIMULATOR END 
%---------------------------------------------------------------------%