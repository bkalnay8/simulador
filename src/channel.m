%---------------------------------------------------------------------%
%						    Simulador QAM-M
%					patricio.reus.merlo@gmail.com
%                       franrainero1@gmail.com
%                      stgoleguizamon@gmail.com
%
% Filename      : channel.m
% Programmers   : Patricio Reus Merlo; Francisco Rainero; Santiago Leguizamon
% Created on	: 7 Jul. 2021
% Description 	: digital communications channel class
% MOD: Reemplazo de switch con todos los campos por un for con un if para
% el caso de normalizar la frecuencia
%---------------------------------------------------------------------%

%---------------------------------------------------------------------%
%                           CHANNEL CLASS 
%---------------------------------------------------------------------%

classdef channel < handle

%---------------------------------------------------------------------%
%                           PRIVATE PROPIETIES
%---------------------------------------------------------------------%

    properties (GetAccess = public, SetAccess = private)
        % -- System settings --
        modulationType      {mustBeInteger  } = 1    ; % (0):PAM o (1)QAM 
        mapperLevels        {mustBePositive ,...
                             mustBeInteger  } = 2    ; 
        oversamplingFactor  {mustBePositive ,...
                             mustBeInteger  } = 8    ;
        symbolRate          {mustBePositive ,...
                             mustBeInteger  } = 32e9

        % -- Noise settings --
        addNoise            {mustBeInteger   } = 0    ; % (0): No 
                                                        % (1): Before channel transfer function
                                                        % (2): After channel transfer function                  
        EbNo_dB             { mustBeReal     } = 1000 ;

        % -- ISI settings --
        bandwidthType       { mustBeInteger  } = 0    ;
        filterLength        { mustBeReal     } = 127  ;
        filterCutFrequency  { mustBePositive ,...
                              mustBeReal     ,... 
                              mustBeLessThan(filterCutFrequency, 1)} = 0.9;
        
    
        
        % -- Filter taps --
        channelTaps = [1 -2]
    end

%---------------------------------------------------------------------%
%                           PUBLIC METHODS
%---------------------------------------------------------------------%

    methods

        %-------------------------------------------------------------%
        %                         CONSTRUCTOR
        %-------------------------------------------------------------%
        function obj = channel(configStruct)

            if nargin == 0
                return
            end

            fields = fieldnames(configStruct);
            for i = 1:length(fields)
                if strcmp(fields{i},'filterCutFrequency')
                    % Calculating digital filter cut frequency from freq value in hz
                    obj.filterCutFrequency = configStruct.filterCutFrequency/(configStruct.oversamplingFactor*configStruct.symbolRate/2);
                else   
                    obj.(fields{i})  =  configStruct.(fields{i});
                end
            end

            % Creates channel filter

            switch obj.bandwidthType 
                case 0 
                    obj.channelTaps = [1];
                case 1
                    obj.channelTaps = fir1(obj.filterLength , obj.filterCutFrequency);
                case 2
                    obj.channelTaps = obj.channelTaps;
            end 

        end

        %-------------------------------------------------------------%
        %                         MAIN FUNCTION
        %-------------------------------------------------------------%

        function ch_out = run ( obj, tx_out)
            
            if obj.bandwidthType && (obj.addNoise == 0)     % Only adds ISI (through channel transfer function)
                ch_out = filter(obj.channelTaps, 1, tx_out);
            elseif obj.bandwidthType && obj.addNoise
                noise = obj.genNoise(var(tx_out), size(tx_out), isreal(tx_out));
                if obj.addNoise == 1
                    ch_out = filter(obj.channelTaps, 1, tx_out + noise);
                else
                    ch_out = filter(obj.channelTaps, 1, tx_out) + noise;
                end
            elseif obj.bandwidthType == 0 && obj.addNoise   % Only adds noise
                noise = obj.genNoise(var(tx_out), size(tx_out), isreal(tx_out));
                ch_out = tx_out + noise;
            else                                            % Does nothing
                ch_out = tx_out;
            end
        end
    end

%---------------------------------------------------------------------%
%                           PRIVATE METHODS
%---------------------------------------------------------------------%
    methods (Access = private)
        function noise = genNoise(obj, var_tx_out, tx_out_size, tx_isreal)
            if obj.modulationType
                SNR_slicer  = 10^(obj.EbNo_dB/10) * log2(obj.mapperLevels^2);
            else
                SNR_slicer  = 10^(obj.EbNo_dB/10) * log2(obj.mapperLevels  );
            end

            SNRChannel = SNR_slicer / obj.oversamplingFactor;
            noisePower = var_tx_out / SNRChannel;

            if tx_isreal
                channelDesviation = sqrt(noisePower/2);
                noise = channelDesviation * randn(tx_out_size);
            else
                channelDesviation = sqrt(noisePower/2);
                realNoise = channelDesviation * randn(tx_out_size) ;
                imagNoise = channelDesviation * randn(tx_out_size) ;
                noise = realNoise + 1j * imagNoise ;    
            end
        end
    end

%---------------------------------------------------------------------%
%                       RECEIVER CLASS END
%---------------------------------------------------------------------%
end