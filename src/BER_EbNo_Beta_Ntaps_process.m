clc; clear all; close all;

% -- Configurables -- %
EbNo_dB = 0.5:0.5:10;
stepSize = [2^(-7), 2^(-8), 2^(-9), 2^(-10), 2^(-11), 2^(-12), 2^(-13), 2^(-14), 2^(-15)];
Ntaps = 21;
M = 4;
analizedBER = 1e-4;
% ------------------- %

stepSizeSweeps = length(stepSize);
NtapsSweeps = length(Ntaps);
EbNoSweeps = length(EbNo_dB);

% -- EbNo vs BER for different FSE step sizes given a certain Ntaps -- %

file_path = "../out/BER_EbNo_NtapsFSE/BER_EbNo_beta_NtapsCteFSE.txt";
file = readtable(file_path);

for j = 0:NtapsSweeps-1
    figure
    semilogy(EbNo_dB, berawgn(file.Var2(1:EbNoSweeps), 'qam', 4).', 'LineWidth', 1.5, 'DisplayName', 'Theorical BER')
    hold on
    for i = 0:stepSizeSweeps-1
        semilogy(file.Var2(1+EbNoSweeps*j+EbNoSweeps*i*NtapsSweeps:EbNoSweeps+EbNoSweeps*j+EbNoSweeps*i*NtapsSweeps), ...
                file.Var3(1+EbNoSweeps*j+EbNoSweeps*i*NtapsSweeps:EbNoSweeps+EbNoSweeps*j+EbNoSweeps*i*NtapsSweeps), ...
                'LineWidth', 1.5, 'DisplayName', "$\beta = " + num2str(file.Var6(1+EbNoSweeps*i*NtapsSweeps), '%e') + ",\ " + num2str(Ntaps(j+1), '%d') + "\ taps$")

        % Calc of approximate EbNo @ analized BER
        [val,idx] = min(abs(file.Var3(1+EbNoSweeps*j+EbNoSweeps*i*NtapsSweeps:EbNoSweeps+EbNoSweeps*j+EbNoSweeps*i*NtapsSweeps) - analizedBER));
        a(j+1, i+1) = idx;
    end

    legend()
    ylabel("Bit-Error Rate (BER)", 'Interpreter', 'latex')
    xlabel("$E_b/N_0$ [dB]", 'Interpreter', 'latex')
    legend('Interpreter', 'latex', 'Location', 'southwest')
    xlim([0.5 10])
    grid on
    set(gcf, 'Position', [100 100 400 400],'Color', 'w')
    saveas(gcf, '../out/BER_EbNo_NtapsFSE/BERvsEbN0StepSizes.svg', 'svg')
end

% -- EbNo Penalty changing FSE step sizes plot -- %

figure
for i = 1:NtapsSweeps
    semilogx(stepSize, EbNo_dB(a(i,:)), '-or', 'LineWidth', 1.5, 'DisplayName', "$FSE\ Ntaps =" + num2str(Ntaps(i), '%d') + "$, $BER=" + num2str(analizedBER, '%.1e') + "$")
    hold on
end

legend('Interpreter', 'latex', 'Location', 'northwest')
ylabel("$E_b/N_0$ [dB]", 'Interpreter', 'latex')
xlabel("FSE step size", 'Interpreter', 'latex')
grid on
set(gcf, 'Position', [100 100 400 400],'Color', 'w')
saveas(gcf, '../out/BER_EbNo_NtapsFSE/FSEstepSizePenalty.svg', 'svg')

%% -- EbNo vs BER for different FSE Ntaps given a certain step size -- %

% -- Configurables -- %
EbNo_dB = 0.5:0.5:10;
stepSize = 2^(-13);
Ntaps = [5, 10, 15, 20, 25, 30, 35, 40];
M = 4;
analizedBER = 1e-5;
% ------------------- %

stepSizeSweeps = length(stepSize);
NtapsSweeps = length(Ntaps);
EbNoSweeps = length(EbNo_dB);

file_path = "../out/BER_EbNo_NtapsFSE/BER_EbNo_NtapsFSE.txt";
file = readtable(file_path);
clear a
for j = 0:stepSizeSweeps-1
    figure
    semilogy(EbNo_dB, berawgn(file.Var2(1:EbNoSweeps), 'qam', 4).', 'LineWidth', 1.5, 'DisplayName', 'Theorical BER')
    hold on
    for i = 0:NtapsSweeps-1
        semilogy(file.Var2(1+EbNoSweeps*i+EbNoSweeps*j*NtapsSweeps:EbNoSweeps+EbNoSweeps*i+EbNoSweeps*j*NtapsSweeps), ...
                file.Var3(1+EbNoSweeps*i+EbNoSweeps*j*NtapsSweeps:EbNoSweeps+EbNoSweeps*i+EbNoSweeps*j*NtapsSweeps), ...
                'LineWidth', 1.5, 'DisplayName', "$\beta = " + num2str(file.Var6(1+EbNoSweeps*i+EbNoSweeps*j*NtapsSweeps), '%e') + "\ " + num2str(Ntaps(i+1), '%d') + "\ taps$")

        % Calc of approximate EbNo @ analized BER
        x = file.Var3(1+EbNoSweeps*i+EbNoSweeps*j*NtapsSweeps:EbNoSweeps+EbNoSweeps*i+EbNoSweeps*j*NtapsSweeps);
        x = x.';

        [val,idx] = min(abs(x - analizedBER));
        a(j+1, i+1) = idx;

    end

    legend()
    ylabel("Bit-Error Rate (BER)", 'Interpreter', 'latex')
    xlabel("$E_b/N_0$ [dB]", 'Interpreter', 'latex')
    legend('Interpreter', 'latex', 'Location', 'southwest')
    xlim([0.5 10])
    grid on
    set(gcf, 'Position', [100 100 400 400],'Color', 'w')
    saveas(gcf, '../out/BER_EbNo_NtapsFSE/BERvsEbN0FSEntaps.svg', 'svg')
end

% -- EbNo Penalty changing FSE step sizes plot -- %

figure
for i = 1:stepSizeSweeps
    plot(Ntaps, EbNo_dB(a(i,:)), '-or', 'LineWidth', 1.5, 'DisplayName', "$FSE\ \beta =" + num2str(stepSize(i), '%d') + "$, $BER=" + num2str(analizedBER, '%.1e') + "$")
    hold on
end

legend('Interpreter', 'latex')
ylabel("$E_b/N_0$ [dB]", 'Interpreter', 'latex')
xlabel("FSE Ntaps", 'Interpreter', 'latex')
grid on
set(gcf, 'Position', [100 100 400 400],'Color', 'w')
saveas(gcf, '../out/BER_EbNo_NtapsFSE/FSEntapsPenalty.svg', 'svg')
