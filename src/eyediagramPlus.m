function varargout = eyediagramPlus(x, n, period, offset, plotstring)

    narginchk(2,5);
    nargoutchk(0,1);
    
    if nargin < 3
        period = 1;
    else
        % convert PERIOD to Double data type if needed
        if ~isa(period, 'float')
            period = double(period);
        end
    end
    
    if nargin < 4
        offset = 0;
    else
        % convert OFFSET to Double data type if needed
        if ~isa(offset, 'float')
            offset = double(offset);
        end
    end
    
    if nargin < 5
        plotstring = 'b-';
    end
    
    % convert N to Double data type if needed
    if ~isa(n,'float')
        n = double(n);
    end
    
    [r,] = size(x);
    
    % flatten input
    if r == 1
        x = x(:);
    end
    
    % Complex number processing
    x = [real(x), imag(x)];
    maxAll = max(max(abs(x)));
    
    % generate normalized time values
    [len_x, wid_x]=size(x);
    t = rem((0 : (len_x-1))/n, 1)';
    
    % wrap right half of time values around to the left
    tmp = find(t > rem((offset/n+0.5),1) + eps^(2/3));
    t(tmp) = t(tmp) - 1;
    
    % if t = zero is at an edge, make it the left edge
    if(max(t)<=0)
        t = t + 1;
    end
    
    % determine the right-hand edge points
    % for zero offset, the index value of the first edge is floor(n/2)+1
    index = fliplr(1+rem(offset+floor(n/2),n) : n : len_x);
    
    % for plotting, insert NaN values into both x and t after each edge point
    % to define the left edge,after the NaNs repeat the ith value of x
    % and insert a value that is (period/n) less than the (i+1)th value of t
    NN = ones(1, wid_x) * NaN;
    for ii = 1 : length(index)
        i = index(ii);
        if i < len_x
            x = [x(1:i,:);   NN;     x(i,:); x(i+1:size(x, 1),:)];
            t = [t(1:i);    NaN; t(i+1)-1/n; t(i+1:size(t, 1))  ];
        end
    end
    
    % adjust the time values to ensure that the x axis remains fixed
    half_n = n/2-1;
    modoffset = rem(offset+half_n,n)-half_n;
    t = rem(t-modoffset/n,1);
    
    % scale time values by period
    t = t*period;
    
    % Create new figure or reuse existing handle
    h = gcf;
    % isNewFig = 0;
    
    % setup for the eye diagram
    limFact = 1;
    sigName = {'I', 'Q'};    
    ha = [-1 -1];
    axo = zeros(2,4);
    
    for idx = [1,2]
        ha(idx) = subplot(1,2,idx);
        plotEye(t, x(:,idx), plotstring, char(sigName(idx)));
        axo(idx,:) = axis;
    end
    yLimits = maxAll * limFact;
    axx=[min(t), max(t), min(-yLimits,min(axo(:,3))), max(yLimits,max(axo(:,4)))];
    
    for idx = [1,2]
        axis(ha(idx),axx);
    end
    
    set(h,'nextplot','replacechildren');
    
    if(nargout == 1)
        varargout(1) = {h};
    end
    
    
    function plotEye(t,x, plotstring, strSubTitle)
    % plotEye does the plotting, and labeling for the eye diagram
    
    plot(t, x, plotstring);
    xlabel('Time','Interpreter','latex','FontSize', 16);
    ylabel('Amplitude','Interpreter','latex','FontSize', 16);
    title(['Eye diagram - Channel ' strSubTitle],'Interpreter','latex','FontSize', 18);
    
    % EOF
    