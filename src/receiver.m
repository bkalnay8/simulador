%---------------------------------------------------------------------%
%						    Simulador QAM-M
% Version modificada de receiver.m de 
% QAM-M_FSE_LMS_simulator
% https://gitlab.com/sfleguizamon/qam-m_fse_lms_simulator
% El ecualizador tiene la opcion de trabajar al baudio (para lo cual hay
% que pasarle los coeficientes del filtro apareado)
%---------------------------------------------------------------------%

%---------------------------------------------------------------------%
%                           RECEIVER CLASS 
%---------------------------------------------------------------------%

classdef receiver < handle

%---------------------------------------------------------------------%
%                           PRIVATE PROPIETIES
%---------------------------------------------------------------------%
    properties (GetAccess = public, SetAccess = private)
        % -- All Rx -- 
        symbolRate           { mustBePositive  , ...
                               mustBeReal      } = 1e9 ; % Baud Rate BR
        oversamplingFactor   { mustBePositive  , ...
                               mustBeInteger   } = 4   ; % Factor de sobremuestreo        
        
        % -- Down-Converter --
        dwConvPhaseError     { mustBeReal      } = 0   ; % err de fase [deg] del down-converter
        dwConvfrequencyOffset{ mustBeReal      } = 0   ; % Offset de frecuencia [Hz] del down converter
        
        
        % -- Anti-alias filter settings --
        antialiasLength      { mustBeReal      } = 127 ;
        antialiasCutFrequency{ mustBePositive  ,...
                              mustBeReal       } = 31.5e9 ;
        antialiasTaps 
        
        % -- Matched Filter -- 
        mfEnable             { mustBeReal      } = 1;
        hTaps
        
        % -- ADC settings --
        antialiasEnable      { mustBeInteger   } = 1   ; % Enable AAFilter
        dspOversamplingFactor{ mustBePositive  , ...
                               mustBeInteger   } = 2   ; % Factor de sobremuestreo del FSE
        adcInitialPhase      { mustBeInteger   } = 0   ; % Fase de muestreo del correlador

        % -- AGC --
        agcOutputLevel       { mustBePositive  } = 1   ;

        % -- FSE --
        FSEtaps
        fseLength            { mustBePositive  , ...
                               mustBeInteger   } = 63   ; % Cantidad de taps del FSE
        saveFilterTaps       {mustBeInteger    ,...     
        mustBeLessThan(saveFilterTaps,2)}        = 1    ; % Save filter taps evolution   

        % -- LMS --
        lmsStepCMA           { mustBeReal      } = 2e-9 ; % Paso del LMS usando CMA
        lmsStepDecisionDriven{ mustBeReal      } = 2e-9 ; % Paso del LMS usando SLICER
        lmsTapsLeakage       { mustBeReal      } = 1e-4 ; % Factor de olvido de los taps del FSE

        % -- CMA --
        forceCMA             { mustBeInteger   } = 0    ; % Al forzar CMA nunca se pasa al SLICER
        symbolsWithCMAEnabled{ mustBeInteger   } = 0    ; % Cant. de simbolos a simular antes de
                                                          % pasar de CMA a DD (SLICER)
        % -- Slicer --
        modulationType       { mustBeInteger   , ...
                            mustBeNonnegative  , ...
            mustBeLessThan(modulationType, 2)  } = 1    ; % Tipo de transmisor (0:PAM o 1:QAM)
        mapperLevels         { mustBePositive  , ...
                               mustBeInteger   } = 2    ; % Cantidad de niveles del mapper M

    end
    
%---------------------------------------------------------------------%
%                           PUBLIC METHODS
%---------------------------------------------------------------------%
    methods

        %-------------------------------------------------------------%
        %                       CONSTRUCTOR METHOD
        %-------------------------------------------------------------%
        
        function obj = receiver(configStruct)

            if nargin == 0
                return
            end

            fields = fieldnames(configStruct);
            
            for i = 1:length(fields)
                if strcmp(fields{i},'antialiasCutFrequency')
                    % Calculating digital filter cut frequency from freq value in Hz
                    obj.antialiasCutFrequency = configStruct.antialiasCutFrequency/(configStruct.oversamplingFactor*configStruct.symbolRate/2);
                else   
                    obj.(fields{i})  =  configStruct.(fields{i});
                end                 
            end

            % Creates antialias filter
            obj.antialiasTaps = fir1(obj.antialiasLength, obj.antialiasCutFrequency);
        end
            
        %-------------------------------------------------------------%
        %                         MAIN METHOD
        %-------------------------------------------------------------%

        function [rx_ak , samplerOutput , fseOutput] = receive (obj, ch_out, CMAReference)            
            
            % -- Anti-alias filter and ADC --
            adc_out = obj.filterAndSample(ch_out) ;

            % -- AGC --
            agc_gain = obj.agcOutputLevel / std(adc_out) ;
            agc_out  = adc_out .* agc_gain ;

            % -- FSE + LMS + CMA --
            inputLength  = length(agc_out);
            outputLength = fix(inputLength / obj.dspOversamplingFactor);

            % Allocate memory
            convolutionBuffer = zeros ( obj.fseLength , 1 ) ; % Column vector
            fseTaps           = zeros ( obj.fseLength , 1 ) ; % FSE coefficients
            fseOutput         = zeros ( inputLength   , 1 ) ; % Filter output  (rate = 2 * baudrate)
            samplerOutput     = zeros ( outputLength  , 1 ) ; % Sampler output (rate = 1 * baudrate)
            rx_ak             = zeros ( outputLength  , 1 ) ; % Slicer output  (rate = 1 * baudrate)
            
            % Coefficients initialization
            fseTaps(floor((obj.fseLength+1) / 2)) = 1 ;

            for sampleIdx = 1 : inputLength
    
                % Pass to DD
                if (sampleIdx < obj.symbolsWithCMAEnabled)
                    CMAEnable = 1 ;
                else
                    CMAEnable = 0 ;
                end
                
                % Update convolution fuffer
                convolutionBuffer (2:end) = convolutionBuffer (1:end-1);
                convolutionBuffer (1) = agc_out(sampleIdx);
                
                % Filter
                fseOutput(sampleIdx) = fseTaps .'* convolutionBuffer ;
                
                % Sampler between FSE and SLICER
                if mod(sampleIdx , obj.dspOversamplingFactor ) == 0
                    symbolIdx = ceil(sampleIdx / obj.dspOversamplingFactor );
                    
                    % Save sample
                    samplerOutput(symbolIdx) = fseOutput(sampleIdx);

                    % Slicer
                    rx_ak(symbolIdx) = obj.slicer(fseOutput(sampleIdx));

                    % Error calculation (err)
                    if CMAEnable || obj.forceCMA
                        err  = fseOutput(sampleIdx) * (abs(fseOutput(sampleIdx)) - CMAReference);
                        step = obj.lmsStepCMA;
                    else
                        err  = fseOutput(sampleIdx) - rx_ak(symbolIdx);
                        step = obj.lmsStepDecisionDriven;
                    end
                    
                    % Update FSE taps
                    if obj.saveFilterTaps
                        obj.FSEtaps(symbolIdx, :) = fseTaps;
                        %obj.FSEtaps(symbolIdx, :) = 0;
                    else
                        obj.FSEtaps(1, :) = fseTaps;
                    end
                    fseTaps = fseTaps * (1 - step * obj.lmsTapsLeakage) - step .* err .* conj(convolutionBuffer);
                end
            end
        end
    end

%---------------------------------------------------------------------%
%                           PRIVATE METHODS
%---------------------------------------------------------------------%

    methods (Access = private)
        
        %-------------------------------------------------------------%
        %                     Anti-Alis and ADC
        %-------------------------------------------------------------%
        function o_adc = filterAndSample (obj, i_adc)
            if obj.antialiasEnable
                i_filtered = filter(obj.antialiasTaps , 1 , i_adc);
            elseif obj.mfEnable
                mf = conj(obj.hTaps(end:-1:1));
                i_filtered = filter(mf, 1, i_adc); 
            else
                i_filtered = i_adc;
            end
            o_adc      = downsample(i_filtered , ...
                                        obj.oversamplingFactor / obj.dspOversamplingFactor ,...
                                            obj.adcInitialPhase ) ;
        end

        %-------------------------------------------------------------%
        %                           SLICER
        %-------------------------------------------------------------%
        
        function o_slicer = slicer(obj, i_slicer)

            input_size = size(i_slicer);
            
            % -- PAM --
            if obj.modulationType == 0
                o_slicer = -1 * ones(input_size);
                o_slicer(i_slicer > 0) = 1 ;
            
            % -- QAM --
            else
                input_real = real(i_slicer);
                input_imag = imag(i_slicer);
            
                ak_I = -1 * ones(input_size);
                ak_Q = -1 * ones(input_size);
                
                switch obj.mapperLevels
                    case 2
                        ak_I(input_real > 0) = 1 ;
                        ak_Q(input_imag > 0) = 1 ;
                        
                    case 4
                        ak_I(input_real >= 2 ) =  3;
                        ak_I(input_real < -2 ) = -3;
                        ak_I(input_real >  0 & input_real < 2) = 1;
                        
                        ak_Q(input_imag >= 2 ) =  3;
                        ak_Q(input_imag < -2 ) = -3;
                        ak_Q(input_imag >  0 & input_imag < 2) = 1;                        
        
                    case 8
                        ak_I(input_real >= 4 ) =  5;
                        ak_I(input_real < -4 ) = -5;
                        ak_I(input_real >= 2 & input_real <  4) =  3;
                        ak_I(input_real < -2 & input_real > -4) = -3;
                        ak_I(input_real >  0 & input_real <  2) =  1;
                        
                        ak_Q(input_imag >= 4 ) =  5;
                        ak_Q(input_imag < -4 ) = -5;
                        ak_Q(input_imag >= 2 & input_real <  4) =  3;
                        ak_Q(input_imag < -2 & input_real > -4) = -3;
                        ak_Q(input_imag >  0 & input_real <  2) =  1;                    
                    
                    otherwise
                        error("The selected modulationType is not supported");
                end
                o_slicer = ak_I + 1j * ak_Q;
            end            
        end
    end

%---------------------------------------------------------------------%
%                       RECEIVER CLASS END
%---------------------------------------------------------------------%
end

